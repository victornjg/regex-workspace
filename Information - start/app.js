/*
Retrieve the starting index for the match, the length of the match and the actual match.
*/

let phrase = "First number: 32, and a second number 100. Here is the last number 15.",
    result,
    regex = /\d+/;

console.log(phrase);

result = regex.exec(phrase);

console.log(result);

console.log('starting index: ', result['index']);
console.log('length of the match: ', result[0].length);
console.log('actual match: ', result[0]);
