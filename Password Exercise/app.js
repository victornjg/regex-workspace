/*
Validate phone numbers entered into the text field. As the number is entered, check to see if it 
matches these formats: (nnn)-nnn-nnnn, nnn.nnn.nnnn, nnn-nnn-nnnn, nnnnnnnnnn, (nnn)nnn-nnnn. If 
the number matches, change the text color from red to green.

Use several different phone numbers to test.

HINT: You can use the keyup event to respond to entered text. There is a CSS Class for red and green.
*/

(() => {
  const input = document.getElementById('password');
  const length = /^.{8,12}$/,
    lowerCaseLetter = /[A-Z]/,
    upperCaseLetter = /[a-z]/,
    number = /[0-9]/,
    specialCharacter = /\W/;

  input.addEventListener('input', (e) => {
    const value = e.target.value;
    if (length.test(value) && lowerCaseLetter.test(value) && upperCaseLetter.test(value) && number.test(value) && specialCharacter.test(value)) {
      e.target.style.color = 'green';
    } else {
      e.target.style.color = 'red';
    }
  });
})();
