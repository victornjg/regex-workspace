/*
Iterate over each match and log the information to the console.
*/

let phrase = "First number: 32, and a second number 100. Here is the last number 15.",
  match,
  regex = /\d+/g;

console.log(phrase);

while (match = regex.exec(phrase)) {
  if (match['index'] == regex.lastIndex) regex.lastIndex++;
  console.log('starting index: ', match['index']);
  console.log('length of the match: ', match[0].length);
  console.log('actual match: ', match[0]);
  console.log('\n');
}
