/*
Iterate through the data provided. Use a regular expression to store the names in a new array but change the order of the name so first name is listed first and last name is last. 
*/

const data = ["Jensen, Dale", "Smith, Andrea", "Jorgensen, Michael", "Vasefi, Annika", "Lopez, Monica", "Crockett, Steven"];
const regex = /^([a-z]+)(?:, )([a-z]+)$/i;
const newData = [];

for (let i = 0; i < data.length; i++) {
  const item = data[i];
  const result = regex.exec(item);
  newData.push(result[2] + ' ' + result[1]);
}
