/*
Create a new array that shows the names with the firstname and then the surname.
*/

let names = ["Smith, James", "Peterson, Alyssa", "Johnson, Lynette", "Lopez, Tony"],
  groupNameRegex = /([a-z]+), ([a-z]+)/i;

console.log(names);

const reversedNames = names.map((name) => name.replace(groupNameRegex, '$2 $1'));

console.log(reversedNames);