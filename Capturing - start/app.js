/*
Extract all the numbers from this phrase and capture those numbers. Then sum the numbers.
*/

let phrase = "First number: 32, and a second number 100. Here is the last number 15.",
  result = 0,
  regex = /\d+/g,
  matches;

console.log(phrase);

//matches = phrase.match(regex).forEach((value) => result += +value);

matches = phrase.match(regex);

console.log(matches);

result = matches.reduce((sum, value) => sum + parseInt(value, 10), 0);

console.log(result);